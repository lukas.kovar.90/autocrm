define('autocrm:find-contacts-handler', ['action-handler'], function (Dep) {

    return Dep.extend({

        actionTest: function (data, e) {
            let entityType = 'Contact';
            emailAddress = this.view.model.attributes.emailAddress;

            this.view.wait(
                this.view.getCollectionFactory().create(entityType)
                    .then(collection => {
                        this.collection = collection;


                        collection.where=[{type:"equals",field:"emailAddress",value:emailAddress}];
                        collection.fetch().then(response => {
                            console.log(response);
                        });
                    })
            );
        },
    });
});